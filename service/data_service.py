import uuid

from sanic.log import logger

from common import const
from common import utils

SCRIPTS = [
    {const.OBJECT_TYPE: [const.OBJ_TYPE_IP_ADDRESS],
     const.SCRIPT_NAME: "run_nmap",
     const.SCRIPT_TYPE: const.SCRIPT_TYPE_SEARCH,
     const.SCRIPT_GROUP: "tools.nmap",
     const.SCRIPT_DESC: "Scan (NMAP)",
     const.SCRIPT_OPTIONS: [
         {const.OPTION_DESC: "Detect OS",
          const.OPTION_NAME: const.NMAP_OS_DETECT,
          const.OPTION_VALUE: "-O",
          },
         # {const.OPTION_DESC: "Detect version",
         #  const.OPTION_NAME: const.NMAP_DETECT_VERSION,
         #  const.OPTION_VALUE: "-sV",
         #  },
         {const.OPTION_DESC: "Script check for vulnerabilities(vulners)",
          const.OPTION_NAME: const.NMAP_SCRIPT_VULNERS,
          const.OPTION_VALUE: "-sV --script vulners",
          },
     ],
     },
    {const.OBJECT_TYPE: [const.OBJ_TYPE_HOST],
     const.SCRIPT_NAME: "get_ext_ip_wgetip",
     const.SCRIPT_TYPE: const.SCRIPT_TYPE_SEARCH,
     const.SCRIPT_GROUP: "tools.ext.wgetip",
     const.SCRIPT_DESC: "Get external IP from wgetip",
     const.SCRIPT_OPTIONS: [
     ],
     },
]


def get_scripts() -> list[dict]:
    return SCRIPTS


async def save_objects_from_event(event: dict) -> tuple[dict, str]:
    new_objects = event.get(const.EVENT_DATA, [])
    saved_objects = save_objects(new_objects)
    return {
               const.DATA_OBJECTS: saved_objects
           }, ""


def save_objects(new_objects: list[dict]) -> list[dict]:
    return [save_object(obj) for obj in new_objects]


def save_object(new_obj: dict) -> dict:
    if new_obj.get(const.OBJ_ID) is None:
        new_obj[const.OBJ_ID] = generate_id(new_obj)
        new_obj[const.OBJ_CREATED] = utils.get_current_date_str()
    return new_obj


def default_id(obj: dict) -> str:
    return f"{obj.get(const.OBJECT_TYPE, '')}|{str(uuid.uuid4())}"


# temporal
ID_FORMATS = {
    const.OBJ_TYPE_FILE: lambda obj: f"{obj.get(const.OBJECT_TYPE, '')}|{obj.get(const.FILE_NAME, '')}",
    const.OBJ_TYPE_IP_ADDRESS: lambda obj: f"{obj.get(const.OBJECT_TYPE, '')}|{obj.get(const.IP_ADDRESS, '')}",
    const.OBJ_TYPE_HOST: lambda obj: f"{obj.get(const.OBJECT_TYPE, '')}|{obj.get(const.IP_ADDRESS, '')}|{obj.get(const.HOST_NAME, '')}",
    const.OBJ_TYPE_PORT: lambda obj: f"{obj.get(const.OBJECT_TYPE, '')}|{obj.get(const.IP_ADDRESS, '')}|{obj.get(const.PORT_NUMBER, '')}",
    const.OBJ_TYPE_SERVICE: lambda obj: f"{obj.get(const.OBJECT_TYPE, '')}|{obj.get(const.IP_ADDRESS, '')}|{obj.get(const.PORT_NUMBER, '')}|{obj.get(const.SERVICE_NAME, '')}",
    const.OBJ_TYPE_CVE: lambda obj: f"{obj.get(const.OBJECT_TYPE, '')}|{obj.get(const.CVE_ID, '')}",
    "": default_id,
}


def generate_id(obj: dict) -> str:
    object_type = obj.get(const.OBJECT_TYPE, "")
    return ID_FORMATS.get(object_type, default_id)(obj)


PROCESSORS = {
    const.EVENT_TYPE_OBJ_CREATE: save_objects_from_event,
}


async def process_event(event: dict) -> tuple:
    logger.info(f"Received event: {event}")
    event_type = event.get(const.EVENT_TYPE, "")

    event_processor = PROCESSORS.get(event_type, utils.unknown_event)

    data, err = await event_processor(event)
    return data, err
