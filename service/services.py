import logging

from sanic.log import logger

from service import script_service, data_service
from common import const, utils


async def get_scripts() -> tuple[dict, str]:
    err = ""
    result = {
        const.DATA_SCRIPTS: data_service.get_scripts()
    }
    return result, err


async def process_event(event: dict) -> tuple:
    logger.info(f"Received event: {event}")

    event_type = event.get(const.EVENT_TYPE, "")

    if event_type == const.EVENT_TYPE_SCRIPTS_GET:
        data, err = await get_scripts()
    elif event_type == const.EVENT_TYPE_SCRIPT_RUN:
        data, err = await script_service.process_event(event)
    elif event_type == const.EVENT_TYPE_OBJ_CREATE:
        data, err = await data_service.process_event(event)
    else:
        data, err = await utils.unknown_event(event)

    return data, err
