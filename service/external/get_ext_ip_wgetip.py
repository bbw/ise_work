import httpx
from sanic.log import logger

from common import const, utils

REGEX_VALID_IP = r"^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"


async def process(event: dict) -> tuple:
    obj = event.get(const.EVENT_DATA, []).pop()  # todo fixme
    err = ""

    res = [obj]

    # if obj.get(const.OBJECT_TYPE) == const.OBJ_TYPE_HOST and obj.get(const.HOST_NAME) == "localhost":
    async with httpx.AsyncClient() as client:
        response = await client.get("http://wgetip.com")
        response_text = response.text
        logger.info(f"response from wget: {response_text}")
        valid_ip_address = utils.check_valid(response_text, REGEX_VALID_IP)
        if valid_ip_address:
            external_ip = {
                const.OBJECT_TYPE: const.OBJ_TYPE_IP_ADDRESS,
                const.IP_ADDRESS: response_text,
                const.LINK_IP_EXTERNAL: obj[const.OBJ_ID]
            }
            res.append(external_ip)
    # else:
    #     err = "obj.type not obj.type.host or host.name not localhost"

    result = {
        const.DATA_OBJECTS: res
    }
    return result, err
