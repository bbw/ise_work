from sanic.log import logger

from service import data_service
from service.external import nmap
from common import const

SCAN_TCP_FIN = "-sF"
SCAN_TCP_SYN = "-sS"
SCAN_TCP_CON = "-sT"
SCAN_UDP_CON = "-sU"
SCAN_PING = "-sP"
SCAN_IDLE = "-sL"
SCAN_PORTS = "-Pn"
SCAN_HOSTS = "-sn"
SCAN_ARP = "-PR"
SCAN_TOP_PORTS = "--top-ports"
SCAN_ALL_PORTS = "-p 0,1-65535"
SCAN_SUBNET = "-p-"
SCAN_LIST = "-sL"

SCRIPTS_DNS_BRUTE = "--script dns-brute.nse"

MOD_DNS_OFF = "-n"
MOD_DETECT_VERSION = "-sV"
MOD_STEALTH_SCAN = "-Pn -sZ"
MOD_DETECT_OS = "-O"


async def process(event: dict) -> tuple:
    logger.info(f"Received scan event: {event}")
    data, err = await run_scan(event)

    result = {
        const.DATA_OBJECTS: data
    }
    return result, err


async def run_scan(event: dict) -> tuple:
    logger.info("Starting scan...")

    scan_targets = event.get(const.EVENT_DATA, [])
    scan_target = scan_targets.pop()  # FIXME

    script_name = event.get(const.SCRIPT_NAME)
    options = event.get(const.SCRIPT_CONFIG)

    scripts = data_service.get_scripts()
    script_options = next(script.get(const.SCRIPT_OPTIONS, []) for script in scripts
                          if script.get(const.SCRIPT_NAME, "") == script_name)

    params = await prepare_params(script_options, options)

    report_file_info, err = await nmap.run(scan_target, params)

    parsed_objects = await nmap.parse_results(report_file_info, scan_target)
    parsed_objects.append(report_file_info)
    return parsed_objects, err


async def prepare_params(script_options: list, enabled_options: dict) -> dict:
    enabled_options_keys = enabled_options.keys()
    params = {
        option.get(const.OPTION_VALUE): enabled_options.get(option.get(const.OPTION_NAME, ""))
        for option in script_options
        if option.get(const.OPTION_NAME, "") in enabled_options_keys
    }
    logger.info(f"script options: {params}")
    # params = {
    #     # MOD_DETECT_OS: "",
    #     MOD_DETECT_VERSION: "",
    #     # SCAN_TOP_PORTS: "1000",
    #     # SCAN_ALL_PORTS: ""
    # }
    return params
