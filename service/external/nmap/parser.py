from sanic.log import logger

from common import const, utils


async def parse_results(report_file_info: dict, scan_target: dict) -> list[dict[str, str]]:
    filename = report_file_info.get(const.FILE_NAME)
    scan_res = utils.read_xml_as_dict(utils.get_local_file(filename))

    parent_id = scan_target.get(const.OBJ_ID, "")
    res_objects = []

    host_tag = scan_res.get("nmaprun", {}).get("host", {})

    address_obj = await parse_address(host_tag, parent_id)
    res_objects.append(address_obj)

    host_obj = await parse_host(host_tag, address_obj, parent_id)
    if len(host_obj) != 0:
        res_objects.append(host_obj)

    ports_tags = host_tag.get("ports", {}).get("port", [])  # wtf?
    for port_tag in ports_tags:
        port_obj = await parse_port(port_tag, address_obj, parent_id)
        res_objects.append(port_obj)

        service_obj = await parse_service(port_tag, port_obj, parent_id)
        res_objects.append(service_obj)

        script_objects = await parse_script(port_tag, port_obj, parent_id)
        res_objects.extend(script_objects)

    # timestamp + job/scan_id
    sorted_objects = sorted(res_objects,
                            key=lambda obj: (obj.get(const.OBJECT_TYPE, ""),
                                             obj.get(const.PORT_NUMBER, "")))
    return sorted_objects


# todo fixme hardcoded for now
async def parse_script(port_tag: dict[str, str], port: dict, parent_id: str) -> list[dict[str, str]]:
    script_tag = port_tag.get("script", {})
    script_name = script_tag.get("@id", "")

    res = []

    outer_table = script_tag.get("table", {})
    cpe_id = outer_table.get("@key", "")
    inner_tables = outer_table.get("table", [])

    if script_name == "vulners":
        for table in inner_tables:
            if isinstance(table, dict):
                cve = await parse_vuln_table(table, cpe_id, script_name, port, parent_id)
                res.append(cve)

    logger.info(f"parsed cves as {repr(res)}")
    return res


async def parse_vuln_table(table_tag: dict, cpe_id:str, script_id: str, port: dict, parent_id: str) -> dict:
    cve_elements = table_tag.get("elem", [])
    cve = {
        const.OBJECT_TYPE: const.OBJ_TYPE_CVE,
        const.SCRIPT_NAME: script_id,
        const.SERVICE_CPE: cpe_id,
        const.LINK_PARENT: parent_id,
        const.PORT_NUMBER: port.get(const.PORT_NUMBER, "")
    }
    for elem in cve_elements:
        key = elem.get("@key", "")
        if key == "is_exploit":
            key = "exploit"
        value = elem.get("#text", "")
        cve[f"{const.OBJ_TYPE_CVE}.{key}"] = value

    logger.info(f"parsed {repr(cve_elements)} as {repr(cve)}")
    return cve



async def parse_service(port_tag: dict[str, str], port: dict, parent_id: str) -> dict[str, str]:
    service_tag = port_tag.get("service", {})
    service_name = service_tag.get("@name", "")
    service_product = service_tag.get("@product", "")
    service_version = service_tag.get("@version", "")
    service_extrainfo = service_tag.get("@extrainfo", "")
    service_ostype = service_tag.get("@ostype", "")
    service_cpe = service_tag.get("cpe", [])

    service_obj = {
        const.OBJECT_TYPE: const.OBJ_TYPE_SERVICE,
        const.SERVICE_TYPE: service_name,
        const.SERVICE_NAME: service_product,
        const.IP_ADDRESS: port.get(const.IP_ADDRESS, ""),
        const.PORT_NUMBER: port.get(const.PORT_NUMBER, ""),
        const.LINK_PARENT: parent_id,
    }

    if service_version != "":
        service_obj[const.SERVICE_VERSION] = service_version

    if service_extrainfo != "":
        service_obj[const.EXTRA_PARAMS] = service_extrainfo

    if service_ostype != "":
        service_obj[const.SERVICE_OS] = service_ostype

    if isinstance(service_cpe, list):
        service_obj[const.SERVICE_CPE] = ", ".join(service_cpe)
    else:
        service_obj[const.SERVICE_CPE] = service_cpe

    return service_obj


async def parse_port(port_tag: dict[str, str], ip_address: dict, parent_id: str) -> dict[str, str]:
    port_num = port_tag.get("@portid", "")
    protocol = port_tag.get("@protocol", "")
    port_obj = {
        const.OBJECT_TYPE: const.OBJ_TYPE_PORT,
        const.PORT_NUMBER: port_num,
        const.PORT_PROTOCOL: protocol,
        const.IP_ADDRESS: ip_address.get(const.IP_ADDRESS),
        const.LINK_PARENT: parent_id
    }
    return port_obj


async def parse_host(host_tag: dict[str, str], ip_address:dict, parent_id: str) -> dict[str, str]:
    hostnames_tag = host_tag.get("hostnames", {})
    host_obj = {}
    if hostnames_tag is not None:
        hostname = hostnames_tag.get("hostname", {}).get("@name", "")
        host_obj = {
            const.OBJECT_TYPE: const.OBJ_TYPE_HOST,
            const.HOST_NAME: hostname,
            const.IP_ADDRESS: ip_address.get(const.IP_ADDRESS),
            const.LINK_PARENT: parent_id
        }
    return host_obj


async def parse_address(host_tag: dict[str, str], parent_id: str) -> dict[str, str]:
    address = host_tag.get("address", {}).get("@addr", "")
    address_type = host_tag.get("address", {}).get("@addrtype", "")
    address_obj = {
        const.OBJECT_TYPE: const.OBJ_TYPE_IP_ADDRESS,
        const.IP_ADDRESS: address,
        const.IP_ADDRESS_TYPE: address_type,
        const.LINK_PARENT: parent_id
    }
    return address_obj
