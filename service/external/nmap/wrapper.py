import shlex

from sanic.log import logger

from common import utils
from common import const
from common.utils import get_current_date_str


NMAP_CMD = "nmap"
REPORT_XML = "-oX"
# BASIC_SCAN = "nmap localhost -oX localhost.xml -sV -p 0,1-65535"


async def run(scan_target: dict, params: dict) -> (dict, str):
    target_address = scan_target.get(const.IP_ADDRESS)
    params_list = [f"{key} {value}" for key, value in params.items()]
    flags = " ".join(params_list)
    datetime_formatted = get_current_date_str()
    print(datetime_formatted)

    report_name = f"{shlex.quote(target_address)}_{datetime_formatted}.xml"
    report_path = utils.get_local_file(report_name)

    command = f"{NMAP_CMD} {target_address} {flags} {REPORT_XML} {report_path}"
    logger.info(f"running command: {command}")

    data, err = await utils.run_cmd(command)

    if err != "":
        return {const.CONSOLE_OUTPUT: data}, err
    if err == "":
        # parsed_data = utils.read_xml_as_dict(report_path)
        report_file_info = {
            const.OBJECT_TYPE: const.OBJ_TYPE_FILE,
            const.LINK_PARENT: scan_target.get(const.OBJ_ID, ""),
            const.FILE_NAME: report_name,
            const.FILE_CONTENT_TYPE: "application/xml",
            const.FILE_SIZE: await utils.get_file_size(report_path)
        }
        return report_file_info, err


