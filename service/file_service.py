from sanic.log import logger

from common import const, utils

PROCESSORS = {
}


async def process_event(event: dict) -> tuple:
    logger.info(f"Received event: {event}")
    event_type = event.get(const.EVENT_TYPE, "")

    event_processor = PROCESSORS.get(event_type, utils.unknown_event)

    data, err = await event_processor(event)
    return data, err
