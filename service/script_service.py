from sanic.log import logger

from service import data_service
from service.external import run_nmap, get_ext_ip_wgetip
from service.internal import process_obj
from common import const
from common import utils

PROCESSORS = {
    # "get_scripts": get_scripts,
    "run_nmap": run_nmap.process,
    "process_obj": process_obj.process,
    "get_ext_ip_wgetip": get_ext_ip_wgetip.process,
    "": utils.unknown_event
}


async def process_event(event: dict) -> tuple[dict, str]:
    logger.info(f"Received event: {event}")
    script_name = event.get(const.SCRIPT_NAME, "")

    event_processor = PROCESSORS.get(script_name, utils.unknown_event)

    data, err = await event_processor(event)
    objects = data.get(const.DATA_OBJECTS, [])
    saved_objects = data_service.save_objects(objects)
    data[const.DATA_OBJECTS] = saved_objects
    return data, err
