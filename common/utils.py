import asyncio
import os
import re

from aiofiles import os as async_os

from sanic.log import logger

from pathlib import Path
from datetime import datetime
from pprint import pprint

import xmltodict

from common import const

DATETIME_ORDERED: str = "%Y%m%d_%H%M%S"
DATA_DIR = "files"


async def run_cmd(command: str) -> (str, str):
    process = await asyncio.create_subprocess_shell(command, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)

    res_data, res_err = "", ""
    try:
        data, err = await process.communicate()
        if 0 != process.returncode:
            res_err = f"Error during command: {command} : {err.decode('utf8')}"
        else:
            res_data = data.decode('utf8').strip()
    except Exception as e:
        res_err = repr(e)

    # Response is bytes so decode the output and return
    return res_data, res_err


def read_xml_as_dict(file_path: Path) -> dict:
    with open(file_path) as file:
        xml_dict = xmltodict.parse(file.read())
        pprint(xml_dict)
        return xml_dict


def get_file_path_ensure_dir(dir_path: Path, filename: str) -> Path:
    logger.info(f"check and make dir : {dir_path}")
    dir_path.mkdir(parents=True, exist_ok=True)
    file_path = dir_path.joinpath(filename)
    return file_path


def parse_int(value: str, default: int = 0) -> int:
    res = default
    logger.info(f"parsing={value} with default={default}")
    try:
        res = int(value)
    except ValueError:
        logger.error(f"cant parse value {value}")
    return res


def get_cpu_cores(default: int) -> int:
    cores = len(os.sched_getaffinity(0))
    return cores if cores > 0 else default


def get_current_date_str() -> str:
    return datetime.now().strftime(DATETIME_ORDERED)


def get_filled(value: str, default_value: str) -> str:
    return value if value is not None and value.strip() != "" else default_value


def parse_env_params(default_params: dict) -> dict[str, str]:
    environ = os.environ

    env_params = {key: get_filled(environ.get(key), default_params.get(key)) for key in default_params.keys()}

    return env_params


def check_valid(value: str, regex: str) -> bool:
    return re.match(regex, value) is not None


def get_project_dir() -> Path:
    project_dir = Path(__file__).parent.parent
    logger.info(f"project dir: {project_dir}")
    return project_dir


def get_local_dir(dir_name: str) -> Path:
    result = Path.joinpath(get_project_dir(), dir_name)
    logger.info(f"using dir: {result}")
    return result


# validate names!
def get_local_file(filename: str) -> Path:
    data_dir = get_local_dir(DATA_DIR)
    filepath = get_file_path_ensure_dir(data_dir, filename)
    # result = Path.joinpath(data_dir, filename)
    logger.info(f"filepath: {filepath}")
    return filepath


async def unknown_event(event: dict) -> tuple[dict, str]:
    return {}, const.ERR_COMMAND_UNKNOWN


async def get_file_size(filepath: Path) -> str:
    file_stat = await async_os.stat(filepath)
    return str(file_stat.st_size)

