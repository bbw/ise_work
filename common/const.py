EVENT_TYPE = "event.type"
EVENT_DATA = "event.data"

DATA_OBJECTS = "data.objects"
DATA_SCRIPTS = "data.scripts"

EVENT_TYPE_OBJ_GET = "event.type.obj.get"
EVENT_TYPE_OBJ_SET = "event.type.obj.set"
EVENT_TYPE_OBJ_CREATE = "event.type.obj.create"
EVENT_TYPE_SCRIPTS_GET = "event.type.scripts.get"
EVENT_TYPE_SCRIPT_RUN = "event.type.script.run"

SCRIPT_NAME = "script.name"
SCRIPT_TYPE = "script.type"
SCRIPT_GROUP = "script.group"
SCRIPT_DESC = "script.desc"
SCRIPT_OPTIONS = "script.options"

SCRIPT_CONFIG = "script.config"
SCRIPT_DATA = "script.data"


OPTION_NAME = "option.name"
OPTION_TYPE = "option.type"
OPTION_VALUE = "option.value"
OPTION_DESC = "option.desc"

DATA_VALUE = "data.value"

RESPONSE_DATA = "resp.data"
RESPONSE_ERROR = "resp.err"

CONSOLE_OUTPUT = "console.out"

SCRIPT_TYPE_SEARCH = "script.type.search"

NMAP_OS_DETECT = "nmap.os.detect"
NMAP_DETECT_VERSION = "nmap.detect.version"
NMAP_SCRIPT_VULN = "nmap.script.vuln"
NMAP_SCRIPT_VULNERS = "nmap.script.vulners"
NMAP_SCRIPT_VULSCAN = "nmap.script.vulscan"


ERR_COMMAND_UNKNOWN = "err.command.unknown"

OBJ_ID = "obj.id"
OBJ_CREATED = "obj.created"

OBJECT_TYPE = "obj.type"

TYPE_ACTIONS = "type.actions"

OBJ_VALUE = "obj.value"

OBJ_TYPE_FILE = "os.file"
OBJ_TYPE_HOST = "network.host"
OBJ_TYPE_PORT = "network.port"
OBJ_TYPE_SERVICE = "network.service"
OBJ_TYPE_IP_ADDRESS = "network.ip.address"
OBJ_TYPE_CVE = "security.cve"

CVE_ID = "security.cve.id"

HOST_NAME = "host.name"

IP_ADDRESS = "ip.address"
IP_ADDRESS_EXTERNAL = "ip.address.external"
IP_ADDRESS_TYPE = "ip.address.type"

PORT_NUMBER = "port.number"
PORT_PROTOCOL = "port.protocol"

SERVICE_TYPE = "service.type"
SERVICE_NAME = "service.name"
SERVICE_VERSION = "service.version"
SERVICE_OS = "service.os"
SERVICE_CPE = "service.cpe"

EXTRA_PARAMS = "extra.params"

LINK_PARENT = "link.parent"
LINK_IP_EXTERNAL = "link.ip.ext"

FILE_NAME = "file.name"
FILE_SIZE = "file.size"
FILE_CONTENT_TYPE = "file.content.type"
