from unittest import IsolatedAsyncioTestCase

from common import utils


class Test(IsolatedAsyncioTestCase):

    async def test_run_nmap(self):
        result, err = await utils.run_nmap("")
        self.assertEqual("", err, "should be empty")
        self.assertNotEqual("", result, "should be something")
