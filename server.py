import json

from sanic import Sanic, Request, response
from sanic.log import logger
from sanic.server.websockets.impl import WebsocketImplProtocol

from service import services
from common import utils
from common import const

SUPERUSER_ENABLED = "SUPERUSER_ENABLED"
HEALTH_ENABLED = "HEALTH_ENABLED"
DEBUG_ENABLED = "DEBUG_ENABLED"
LOG_ENABLED = "LOG_ENABLED"
SERVER_NAME = "SERVER_NAME"
SERVER_PORT = "SERVER_PORT"
SERVER_WORKERS = "SERVER_WORKERS"

DEFAULT_SUPERUSER_ENABLED = "false"
DEFAULT_HEALTH_ENABLED = "true"
DEFAULT_DEBUG_ENABLED = "true"
DEFAULT_LOG_ENABLED = "true"
DEFAULT_SERVER_NAME = "server"
DEFAULT_SERVER_PORT = "8000"
DEFAULT_SERVER_WORKERS = str(utils.get_cpu_cores(4))

DEFAULT_PARAMS = {
    SUPERUSER_ENABLED: DEFAULT_SUPERUSER_ENABLED,
    DEBUG_ENABLED: DEFAULT_DEBUG_ENABLED,
    LOG_ENABLED: DEFAULT_LOG_ENABLED,
    HEALTH_ENABLED: DEFAULT_HEALTH_ENABLED,
    SERVER_NAME: DEFAULT_SERVER_NAME,
    SERVER_PORT: DEFAULT_SERVER_PORT,
    SERVER_WORKERS: DEFAULT_SERVER_WORKERS,
}


def create_response(data, err):
    resp_body = {
        const.RESPONSE_DATA: data,
        const.RESPONSE_ERROR: err
    }
    logger.info(f"Response body: {resp_body}")
    return resp_body


# @app.route("/command", methods=[HTTPMethod.POST])
# async def post_command(request: Request):
#     command = request.json
#
#     data, err = await commands.run(command)
#
#     resp_body = create_response(data, err)
#     return json_web_resp(resp_body, HTTPStatus.OK)


async def process_event(event: dict) -> dict:
    logger.info(f"Received event: {event}")

    data, err = await services.process_event(event)

    resp_body = create_response(data, err)
    return resp_body


# @app.websocket("/websocket")
async def ws_channel(request: Request, ws: WebsocketImplProtocol):
    while True:
        data = {"socket.status": "connected"}
        await ws.send(json.dumps(data))

        received_message = await ws.recv()
        received_event = json.loads(received_message)

        logger.info(f"Received: {received_event}")

        resp = await process_event(received_event)

        await ws.send(json.dumps(resp))

        await ws.send(json.dumps({const.RESPONSE_DATA: "finished"}))


# todo fileid/hash
async def file_download_options(request: Request):
    headers = {
        "Access-Control-Allow-Origin": "*",  # todo fixme
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
    }
    return response.json(
        body={},
        headers=headers,
        status=200
    )


async def file_download(request: Request):
    logger.info(f"Received: {request}")
    json_request = request.json
    filename = json_request.get(const.FILE_NAME)   # todo validate
    filetype = json_request.get(const.FILE_CONTENT_TYPE)   # todo validate
    filesize = json_request.get(const.FILE_SIZE)   # todo validate
    file_path = utils.get_local_file(filename)

    headers = {
        "Content-Disposition": f'Attachment; filename="{filename}"',
        "Content-Type": filetype,
        "Content-Length": filesize,
        "Access-Control-Allow-Origin": "*",  # todo fixmes
    }

    # return await response.file_stream(
    return await response.file(
        file_path,
        headers=headers,
        status=200
    )


def create_server(params: dict[str, str]):
    app = Sanic(params.get(SERVER_NAME))

    app.config.CORS_ORIGINS = "*"

    app.config[SUPERUSER_ENABLED] = params.get(SUPERUSER_ENABLED) == "true"
    # app.config.HEALTH = params.get(HEALTH_ENABLED) == "true"

    app.add_websocket_route(ws_channel, "/websocket")
    app.add_route(file_download_options, "/api/file/download", methods=["OPTIONS"])
    app.add_route(file_download, "/api/file/download", methods=["GET", "POST"])

    debug = params.get(DEBUG_ENABLED) == "true"
    logging = params.get(LOG_ENABLED) == "true"

    app.run(host="0.0.0.0",
            debug=debug,
            access_log=logging,
            port=utils.parse_int(params.get(SERVER_PORT)),
            workers=utils.parse_int(params.get(SERVER_WORKERS)))


if __name__ == '__main__':

    server_params = utils.parse_env_params(DEFAULT_PARAMS)

    # settings from env with defaults
    create_server(server_params)
