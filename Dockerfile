FROM python:3-slim as builder

EXPOSE 8000

ENV PYTHONUNBUFFERED=1 \
    PIP_DEFAULT_TIMEOUT=200 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_NO_CACHE_DIR=off \
    POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_CREATE=false \
    POETRY_VERSION=1.2.1 \
    PATH="$PATH:/root/.local/bin"
    #fix paths for poetry

RUN python3 -m ensurepip
RUN pip3 install --upgrade pip setuptools pipx
RUN pipx install poetry==$POETRY_VERSION

WORKDIR /src

COPY pyproject.toml poetry.lock /src/

#RUN poetry config virtualenvs.create false && poetry export --dev --without-hashes --no-interaction --no-ansi -f requirements.txt -o requirements.txt
RUN poetry export --with dev --without-hashes --no-interaction --no-ansi -f requirements.txt -o requirements.txt

RUN pip install --prefix=/runtime --force-reinstall -r requirements.txt

#FROM python:3-slim as production
FROM python:3.10-alpine as production

#RUN apt-get update && apt-get install nmap git -y
RUN apk add nmap nmap-scripts git

RUN pip3 install uvloop

#RUN git clone https://github.com/scipag/vulscan.git /usr/share/nmap/scripts/vulscan

COPY --from=builder /runtime /usr/local
COPY . /app
WORKDIR /app

#todo cron?
#Update CVE databases
#CMD ["/bin/bash","updateFiles.sh"]

CMD ["python", "server.py"]